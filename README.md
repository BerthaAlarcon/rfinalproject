# Pre-erquisites

* Create a account in gitlab.com
* Create & install your ssh keys into your system
* `git clone git@gitlab.com:BerthaAlarcon/rfinalproject.git`
* Pre-install those libraries we'll use into your r image:

```r
install.packages("lubridate")
install.packages("corrplot")
```


