## Load the libraries
library(stats)
library(readr)
library(dplyr, warn.conflicts = FALSE)

## Load the dataset from working directory 
kc_house_data <- read_csv("kc_house_data.csv")

#function to describe the dataset
describe_dataset <- function(kc_dataset)
{
  ##describe the dataset -> Descriptive Analytics
  print("#################################")
  print("Dimensions")
  print("#################################")
  print(dim(kc_dataset))
  print("#################################")
  print("Names")
  print("#################################")
  print(names(kc_dataset))
  print("#################################")
  print("Length")
  print("#################################")
  print(length(kc_dataset))
  print("#################################")
  print("Classes")
  print("#################################")
  print(str(kc_dataset))
  print("#################################")
  print("Top 5 rows")
  print("#################################")
  print(head(kc_dataset))
  print("#################################")
  print("Bottom 5 rows")
  print("#################################")
  print(tail(kc_dataset))
  print("#################################")
  print("Summary")
  print("#################################")
  print(summary(kc_dataset))
  
}

remove_outliers <- function(kc_dataset)
{
  #Before removing outliers
  boxplot(kc_dataset$price, main = paste("Boxplot: Price with outliers."))
  plot(kc_dataset$sqft_living, kc_dataset$price, main = paste("Scatterplot: Price ~ sqft_living with outliers."), xlab="sqft_livig", ylab="Price")
  abline(h=mean(kc_dataset$price), col="blue")
  abline(v=mean(kc_dataset$sqft_living), col="blue")
  abline(lm(kc_dataset$price~kc_dataset$sqft_living),col="orange")
  abline(h=mean(kc_dataset$price) + sd(kc_dataset$price)*1.96, col="red")
  abline(h=mean(kc_dataset$price) - sd(kc_dataset$price)*1.96, col="red") 
  abline(h=summary(kc_dataset$price)[2], col="green")
  abline(h=summary(kc_dataset$price)[5], col="green")
  #Add the line with the Median
  abline(h=summary(kc_dataset$price)[3], col = "Purple")
  
  hist(kc_dataset$price,main = paste("Histogram: Price with outliers."))
  
  stdv <- sd(kc_dataset$price)
  upper <- mean(kc_dataset$price) + stdv * 1.96
  lower <- mean(kc_dataset$price) - stdv * 1.96
  
  kc_dataset <- dplyr::filter(kc_dataset, kc_dataset$price >= lower & kc_dataset$price <= upper)
  
  #After removing outliers
  boxplot(kc_dataset$price, main = paste("Boxplot: Price with NO outliers."))
  plot(kc_dataset$sqft_living, kc_dataset$price, main = paste("Scatterplot: Price ~ sqft_living with NO outliers."), xlab="sqft_livig", ylab="Price")
  abline(h=mean(kc_dataset$price), col="blue")
  abline(v=mean(kc_dataset$sqft_living), col="blue")
  abline(lm(kc_dataset$price~kc_dataset$sqft_living),col="orange")
  abline(h=mean(kc_dataset$price) + sd(kc_dataset$price)*1.96, col="red")
  abline(h=mean(kc_dataset$price) - sd(kc_dataset$price)*1.96, col="red") 
  abline(h=summary(kc_dataset$price)[2], col="green")
  abline(h=summary(kc_dataset$price)[5], col="green")
  #Add the line with the Median
  abline(h=summary(kc_dataset$price)[3], col = "Purple")
  
  hist(kc_dataset$price,main = paste("Histogram: Price with NO outliers."))
  
  return(kc_dataset)
}

##Functions
#Retrieve a subset of the dataset by zipcode
get_zipcode_subset <- function(mzipcode)
{
  ##Select the subset based on the zipcode
  zip_subset <- dplyr::filter(kc_dataset, kc_dataset$zipcode == mzipcode )
  return(zip_subset)
}

plot_price_sqftliving_per_zipcode <- function(mzipcode)
{
  zip_subset <- get_zipcode_subset(mzipcode)
  
  boxplot(zip_subset$price, main = paste(mzipcode, " with NO outliers."))
  
  plot(zip_subset$sqft_living, zip_subset$price, main = paste(mzipcode, " with NO outliers."), xlab="sqft_living", ylab = "Price")
  #Add a regression line
  abline(lm(zip_subset$price ~ zip_subset$sqft_living), col="Orange")
  #Add the min and max boundaries
  abline(h=summary(zip_subset$price)[1], col = "Yellow")
  abline(h=summary(zip_subset$price)[6], col = "Yellow")
  #Add the limits for +/- 3 times the standard deviation
  abline(h=mean(zip_subset$price) + sd(zip_subset$price)*1.96, col = "Red")
  abline(h=mean(zip_subset$price) - sd(zip_subset$price)*1.96, col = "Red")
  #Add the line for the mean
  abline(h=mean(zip_subset$price), col = "Blue")
  abline(v=mean(zip_subset$sqft_living), col = "Blue")
  #Add the line for the 1st and 3rd quartile
  abline(h=summary(zip_subset$price)[2], col = "Green")
  abline(h=summary(zip_subset$price)[5], col = "Green")
  #Add the line with the Median
  abline(h=summary(zip_subset$price)[3], col = "Purple")
  
  hist(zip_subset$price, main = paste(mzipcode, " with NO outliers."))
}

plot_estimated_price_per_bedroom_bathroom_sqft_by_zipcode <- function(mBedroomIni, mBedroomEnd, mBathroomIni, mBathroomEnd, msqftIni, msqftEnd)
{
  zip_subset <- dplyr::filter(kc_dataset, (kc_dataset$bedrooms >= mBedroomIni & kc_dataset$bedrooms <= mBedroomEnd) & (kc_dataset$bathrooms >= mBathroomIni & kc_dataset$bathrooms <= mBathroomEnd) & (kc_dataset$sqft_living >= msqftIni & kc_dataset$sqft_living <= msqftEnd))
  
  avg_price_sqft_by_zipcode <- aggregate(zip_subset$price, list(zip_subset$zipcode),mean)
  
  title <- paste("Average Price by Zipcode")
  msub <- paste( mBedroomIni," <= Bedroom <= ",mBedroomEnd," & ", mBathroomIni, " <= Bathroom <= ", mBathroomEnd," & " ,msqftIni, " <= sqft_living <= ", msqftEnd)
  
  plot(avg_price_sqft_by_zipcode, type = "b",main = title, sub = msub, xlab = "Zipcode", ylab = "Price")
  hist(avg_price_sqft_by_zipcode$x, main = title, xlab = "Price", ylab = "Frequency")
  
  return(avg_price_sqft_by_zipcode)
}

main <- function()
{
  ## Get a list of unique zip codes
  zipcodes <- unique(kc_dataset$zipcode)
  
  ## Loop through all zip codes i the dataset
  for(zipcode in zipcodes)
  {
    #plot a boxplot, scatterplot and histogram of the price as a function of sqft_living per zipcode
    plot_price_sqftliving_per_zipcode(zipcode)
  }
  describe_dataset(kc_dataset)
}

## Enhance the dataset with some data transformation
kc_dataset <- dplyr::select(kc_house_data,date, price,bedrooms, bathrooms, sqft_living, condition, grade, zipcode, lat, long, yr_built)
#remove outliers from the dataser
kc_dataset <- remove_outliers(kc_dataset)

main()
#plot_estimated_price_per_bedroom_bathroom_sqft_by_zipcode(2,3,2,4,1000,2000)
